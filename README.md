# Voicebot Junior Java Developer - Recruitment Task

## Task:
Napisz program który odczyta dane z pliku "employees.json" oraz "employees.csv" i na ich podstawie zwróci sumę zarobków dla poszczególnych stanowisk pracy. (Osobno dla pliku employees.csv oraz osobno dla pliku employees.json). 

Dla przykładu:
```
Janitor - 2345,23
Teacher - 200,00
...
```

Oceniane będą czystość, przejrzystość oraz działanie kodu, architektura oraz napisane testy.
