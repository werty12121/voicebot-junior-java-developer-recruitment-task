package com.werty12121.voicebot_recruitment_task.Data;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CSVReaderTest {
    @Test
    void cleanShouldRemoveAllDoubleQuotes() {
        String actualValue = CSVReader.clean("\"test\"");
        assertEquals("test", actualValue);
    }

    @Test
    void cleanShouldTrimString() {
        String actualValue = CSVReader.clean("    \"test\"       ");
        assertEquals("test", actualValue);
    }

    @Test
    void toJSONShouldTreatFirstRowAsColumnNames() {
        List<List<String>> records = new ArrayList<>();
        List<String> column_names = new ArrayList<>();
        column_names.add("c1");

        List<String> values = new ArrayList<>();
        values.add("v1");

        records.add(column_names);
        records.add(values);

        JSONObject actualValue = (JSONObject) CSVReader.toJSON(records).get(0);
        assertTrue(actualValue.keySet().contains("c1"));

    }

    @Test
    void toJSONShouldNotUseFirstColumnAsData() {
        List<List<String>> records = new ArrayList<>();
        List<String> column_names = new ArrayList<>();
        column_names.add("c1");

        List<String> values = new ArrayList<>();
        values.add("v1");

        records.add(column_names);
        records.add(values);

        int actualValue = CSVReader.toJSON(records).size();
        assertEquals(1, actualValue);

    }
}
