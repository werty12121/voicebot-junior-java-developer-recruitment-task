package com.werty12121.voicebot_recruitment_task.Data;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVReader {
    static String clean(String text) {
        text = text.replace('\"', ' '); //remove " from text
        text = text.trim();
        return text;
    }

    static JSONArray toJSON(List<List<String>> records) {
        JSONArray result = new JSONArray();
        List<String> columnNames = records.get(0);
        records.remove(0);
        for (List<String> row : records) {
            JSONObject temp_row = new JSONObject();
            for (int fieldId = 0; fieldId < row.size(); fieldId++) {
                String key = columnNames.get(fieldId);

                String value = row.get(fieldId);
                value = CSVReader.clean(value);

                temp_row.put(key, value);
            }
            result.add(temp_row);
        }
        return result;
    }

    public static JSONArray read_file(String path, String splitChar) throws IOException {
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(splitChar);
                records.add(Arrays.asList(values));
            }
        }
        return CSVReader.toJSON(records);
    }
}
