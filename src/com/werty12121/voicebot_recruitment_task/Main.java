package com.werty12121.voicebot_recruitment_task;

import com.werty12121.voicebot_recruitment_task.Data.CSVReader;
import com.werty12121.voicebot_recruitment_task.Data.JSONReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    JSONArray employees;
    JSONObject result;

    public Main() {
        this.result = new JSONObject();
    }

    public static void main(String[] args) {
        Main main = new Main();
        System.out.println("Enter path to .csv or .json file");
        main.run();
        System.out.println("Enter path to .csv or .json file");
        main.run();
    }

    private String getInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    void readData(String path) throws IOException, ParseException {
        String file_extension = path.split("\\.")[path.split("\\.").length - 1];
        switch (file_extension) {
            case "csv": {
                this.employees = CSVReader.read_file(path, ";");
                break;
            }
            case "json": {
                JSONObject employees_obs = JSONReader.read_file(path);
                this.employees = (JSONArray) employees_obs.get("employees");
                break;
            }
            default: {
                throw new IOException("Not implemented file extension");
            }
        }
    }

    Float rawFloatToString(String floatText) {
        floatText = floatText.replace(',', '.');
        return Float.parseFloat(floatText);
    }

    void calculateSumOfSalaries() {
        for (Object employee : this.employees) {
            JSONObject employee_temp = (JSONObject) employee;

            String salary_str = (String) employee_temp.get("salary");
            Float salary = rawFloatToString(salary_str);

            String job = (String) employee_temp.get("job");

            Float previous_sum = (Float) this.result.getOrDefault(job, 0.0f);

            this.result.put(job, previous_sum + salary);

        }
    }

    void clean() {
        this.employees = new JSONArray();
        this.result = new JSONObject();
    }

    private void show() {
        for (Object key : this.result.keySet()) {
            System.out.println(key.toString() + ": " + this.result.get(key));
        }
    }

    public void run(String path) {
        this.clean();
        try {
            this.readData(path);
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
            return;
        }
        this.calculateSumOfSalaries();
        this.show();
    }

    public void run() {
        String path = this.getInput();
        this.run(path);
    }

}
