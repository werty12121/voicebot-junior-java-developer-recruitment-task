package com.werty12121.voicebot_recruitment_task;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MainTest {
    @Test
    void cleanShouldOverwriteValues() {
        Main testObject = new Main();
        testObject.employees = null;
        testObject.result = null;

        testObject.clean();

        assertEquals(testObject.result, new JSONObject());
        assertEquals(testObject.employees, new JSONArray());
    }

    @Test
    void rawFloatToStringShouldThrowErrorIfStringIsNotConvertible() {
        Main testObject = new Main();

        assertThrows(NumberFormatException.class, () -> testObject.rawFloatToString("asd"));
    }

    @Test
    void rawFloatToStringShouldConvertIntegerAsFloat() {
        Main testObject = new Main();
        Float actualValue = testObject.rawFloatToString("1");

        assertEquals(1f, actualValue);
    }

    @Test
    void rawFloatToStringShouldReplaceAllCommasWithDots() {
        Main testObject = new Main();
        Float actualValue = testObject.rawFloatToString("1,567");

        assertEquals(1.567f, actualValue);
    }

    @Test
    void readDataShouldThrowErrorIfFileNotExist() {
        Main testObject = new Main();

        assertThrows(IOException.class, () -> testObject.readData("file.csv"));
    }

    @Test
    void readDataShouldThrowErrorIfFileHasDifferentExtension() {
        Main testObject = new Main();

        assertThrows(IOException.class, () -> testObject.readData("file.somerandomextension"));
    }


}
